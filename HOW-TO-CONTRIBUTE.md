[![Cartography Playground](https://moritzbru.gitlab.io/cartography-playground/assets/cartography-playground-logo-line.svg)](https://moritzbru.gitlab.io/cartography-playground/)
# How to contribute

**Requirements**
* [GitLab account](https://gitlab.com/users/sign_in) [_optional_]
* Git GUI (e.g. [GitKraken](https://www.gitkraken.com/)) [_optional_]
* [Node.js](https://nodejs.org/en/)
* [Ruby](https://www.ruby-lang.org/en/downloads/)
* Text editor (e.g. [Atom](https://atom.io/))

## Working with git

If you do **not** want to work with Git and GitLab you can just [download the project](https://gitlab.com/MoritzBru/cartography-playground/-/archive/master/cartography-playground-master.zip) and continue working just with your local copy offline. However, this has two drawbacks:
1. You have to pull the latest updates manually and probably override your local changes
1. You cannot send a pull request and contribute to the project by adding your playground

Now that you are convinced to work with Git, it is probably best to download a Git GUI like for example [GitKraken](https://www.gitkraken.com/).

If you don't have one yet, now is the time to create a [GitLab account](https://gitlab.com/users/sign_in).

Now go to the [Cartography Playground GitLab Repository](https://gitlab.com/MoritzBru/cartography-playground) and <kbd>Fork</kbd> the project.

In GitKraken *Clone Repo*, log in to GitLab and clone your Fork of Cartography Playground to a local folder on your computer.

## Preparing a local build environment

If it is not already installed, install [Node.js](https://nodejs.org/en/) and [Ruby](https://www.ruby-lang.org/en/downloads/).

Open a terminal in the root folder `cartography-playground`.

> Hint: On Windows this can be done by pressing <kbd>shift</kbd> + right-clicking in an empty spot in the folder and selecting *open command/powershell window here* from the menu.

Run the command `bundle` to download all dependencies (e.g. [Jekyll](https://jekyllrb.com/) etc.) that are needed for the project.

Now you can build the project and run it on a local server by typing `bundle exec jekyll serve`.
Amongst other things, the output should print somethings like `Server address: http://127.0.0.1:4000/cartography-playground/` so go to that address in your preferred Browser (Internet Explorer is not supported). You should now see the Cartography Playground website served locally. As long as the server is running it updates automatically when you change some source files.
> Hint: If you do not see your most recent changes you might have to force reload (= clear cache and reload) the page by pressing <kbd>ctrl</kbd>+<kbd>F5</kbd> in your browser.

## Getting to know the project

The folder structure of the project should look something like this:

```yaml
cartography-playground
│   404.html
│   Gemfile # includes Ruby dependencies like Jekyll
│   Gemfile.lock
│   index.html
│   legal-notice.html
│   README.md
│   site.webmanifest
│   sw.js
│   _config.yml # Jekyll configuration
│   
├───assets # contains image assets
│       cartography-playground-logo-line.svg
│       cartography-playground-logo-square.svg
│       og-image.png
│       
├───css
│   │   style.scss
│   │   topography-dark.svg
│   │   topography-light.svg
│   │   
│   └───_scss
│       ├───bootstrap
│       │       bootstrap.scss
│       │       ... # bootstrap style scss files
│       │           
│       └───custom
│               ... # custom style scss files
│               
├───favicon
│       favicon.ico
│       ... # favicons for different platforms
│       
├───search
│       search.html
│       search.js
│       search.json # searchdatabase
│       
├───_includes # Parts that are included elsewhere
│   │   head.html
│   │   footer.html
│   │   js.html
│   │   ... # Jekyll includes (HTML)
│   │   
│   └───svg
│           404.svg
│           logo.svg
│           ... # Jekyll includes (Inline SVG)
│           
├───_layouts # Layout templates for Jekyll
│       compress.html
│       default.html
│       playground.html
│       
├───_plugins # Ruby plugins
│       expand_nested_variable_filter.rb
│       js_compressor.rb
│       
└───_playgrounds # Each playground has its own subfolder here
    ├───cartographic-generalization
    │       ... # Playground files
    │           
    ├───clustering-comparison
    │       ... # Playground files
    │           
    ├───contour-lines-to-profile
    │       ... # Playground files
    │           
    ├───douglas-peucker-algorithm
    │       ... # Playground files
    │           
    ├───map-design
    │       ... # Playground files
    │           
    └───quiz
            ... # Playground files
```

Cartography Playground uses [Jekyll](https://jekyllrb.com/) and it's templating language [Liquid](https://shopify.github.io/liquid/), which can be categorized into *objects*, *tags*, and *filters*. *objects* are written in  double curly braces `{{ ... }}` and contain variables that become the pages content. *filters* are appended to *objects* separated by `|` and change their outputs. *tags* are denoted by curly braces with percentage signs `{% ... %}` and contain control flow and logic.

```html
<!-- object -->
{{ page.title }}
<!-- Output: Cartography Playground -->

<!-- filter -->
{{ page.title | upcase | prepend: "Hello " }}
<!-- Output: Hello CARTOGRAPHY PLAYGROUND -->

<!-- tag -->
{% for playground in site.playgrounds %}
  <h1>{{ playground.title }}</h1>
  <p>{{ playground.description }}</p>
{% endfor %}
<!-- Loop through all playgrounds and print their title as heading and description as paragraph -->
```

As you can see in the examples above, [Liquid](https://shopify.github.io/liquid/) is used to extend `HTML` or `Markdown` markup which contains the content of the websites. Other languages used for Cartography Playground are `CSS/SCSS` for styling the content, `SVG` for displaying nice vector images, `JavaScript` for adding interactivity to the pages and `Ruby` for extending Jekyll at compilation time.

## Adding a new playground

If you want to add a new playground you have to create a new folder inside the `_playgrounds` folder. Name your folder the same as your content file. The name should be clear and short and not include umlauts or other special characters and have dashes instead of spaces because that name will be the url of the new playground. The content file can either be an `HTML`-file or for easier writing a `Markdown`-file. Have a look at the [Markdown-Cheatsheet](https://www.markdownguide.org/cheat-sheet) for infos on how to write `Markdown`.

### Frontmatter

Your content file then need some `frontmatter` which is written in `YAML` between three dashes in the first lines of the file:
* title
  * obligatory
  * nice Title for the page that gets displayed on top of the page and on the card for the playground selection on the homepage
  * use quotes `"..."`
* description
  * obligatory
  * short description that gets displayed on the card for the playground selection
  * use quotes `"..."`
* date
  * obligatory
  * date when file was created
  * format: `yyyy-mm-dd`
  * used for sorting playground on the homepage
* tags
  * obligatory
  * tags used for the search searchdatabase
  * tags separated by space, no commas between tags
  * no quotes needed
* references
  * optional
  * references for citations or further information
  * format: see example
* head_additional
  * optional
  * html to be included in the head section
  * usually used for including additional Stylesheets
* foot_additional
  * optional
  * html to be included at the end of the body section
  * usually used for including additional Scripts

**Example**

```yaml
---
title: "New Playground Example"
description: "This is an awesome new playground example to display how everything works."
date: 2018-07-31
tags: example new playground contribute guide
references:
  - author: "Albert Einstein"
    title: "Can quantum-mechanical description of physical reality be considered complete?"
    date: May 1935
    link: https://journals.aps.org/pr/pdf/10.1103/PhysRev.47.777
  - author: "Wikipedia"
    title: "Cartography"
    date: July 2018
    link: https://en.wikipedia.org/wiki/Cartography
foot_additional: >
  {% include mathjax.html %}
  {% include mapbox.html %}
  <script src="{{ 'new-playground.js' | prepend: page.url | absolute_url }}"></script>
---
```

### Bootstrap

Cartography Playground uses [Bootstrap](https://getbootstrap.com/) so you can also use all the [Bootstrap components](https://getbootstrap.com/docs/4.1/components/alerts/) for laying out your page.
> Hint: you can also use `HTML`-snippets inside of a `Markdown`-file.

### MathJax

If you want your site to have beautiful math formulas you can include [MathJax](https://www.mathjax.org/) by adding `{% include mathjax.html %}` to your `foot_additional` and then write LaTeX formulas between `\( ... \)` for inline math and `\[ ... \]` for display math.

**Example**

```tex
The general quadratic equation is
\[ ax^2 + bx + c = 0 \]
where \( x \) represents the unknown variable and \( a \), \( b \) and \( c \) are constants with \( a \neq 0 \).
Solved for \( x \) the quadratic formula is
\[ x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a} \]
```

### Mapbox

For including a map in your page, you have to add `{% include mapbox.html %}` to your `foot_additional`, first. Then you need to create a container for your map by adding `<div id="map"></div>` where you want your map to appear. Finally, you have to create a `JavaScript` file in your new playground folder and include that also in your `foot_additional` like this `<script src="{{ 'NAME-OF-YOUR-JS-FILE.js' | prepend: page.url | absolute_url }}"></script>`. Inside your `.js` file you nee to initialize the map:

```javascript
mapboxgl.accessToken = mapbox_accessToken;
var map = new mapboxgl.Map({
  container: "map",
  style: "mapbox://styles/mapbox/streets-v10"
});
```

You can of course include [more options](https://www.mapbox.com/mapbox-gl-js/api/#map), [other styles](https://www.mapbox.com/api-documentation/#styles), or [other interaction methods](https://www.mapbox.com/mapbox-gl-js/example/simple-map/) as you like.

### Scalable Vector Graphics

Cartography Playground uses only `SVG`s, no raster images. You can either use [SVG.js](http://svgjs.com/) by including `{% include svgjs.html %}` in your `foot_additional` and create the `SVG`s programmatically with `JavaScript` or create the `SVG`s in your favorite graphics editor (e.g. [Inkscape](https://inkscape.org/en/)), save them in your new playground folder and include them in your page for example via `img`-tag like

```html
<img class="img-fluid mx-auto d-block" src="NAME-OF-YOUR-SVG.svg" alt="ALTERNATIVE TITLE FOR YOUR SVG">
```

or include them inline with Jekyll like

```liquid
{% include_relative NAME-OF-YOUR-SVG.svg %}
```

To make sure your image fits in the design properly use the following colors:

|                         #f8f9fa                          |                         #343a40                          |                         #0065bd                          |                         #a2ad00                          |                         #e37222                          |                         #e5e3da                          |
|:--------------------------------------------------------:|:--------------------------------------------------------:|:--------------------------------------------------------:|:--------------------------------------------------------:|:--------------------------------------------------------:|:--------------------------------------------------------:|
| ![#f8f9fa](https://via.placeholder.com/64/f8f9fa/f8f9fa) | ![#343a40](https://via.placeholder.com/64/343a40/343a40) | ![#0065bd](https://via.placeholder.com/64/0065bd/0065bd) | ![#a2ad00](https://via.placeholder.com/64/a2ad00/a2ad00) | ![#e37222](https://via.placeholder.com/64/e37222/e37222) | ![#e5e3da](https://via.placeholder.com/64/e5e3da/e5e3da) |


### Other Libraries

Of course you can use other libraries as you like. If you include other dependencies, try to include them from a content delivery network - preferably [CDNJS](https://cdnjs.com/). Please also add the new library to the dependecies in the [README.md](https://gitlab.com/MoritzBru/cartography-playground/blob/master/README.md) of the project to make it easier to maintain and keep track of the dependencies.

## Publishing the new playground

If you want your changes to apear on the *original* Cartography Playground `commit` and `push` your change to your fork of of the project on GitLab and send a `pull/merge request`.

If something gets merged, you can follow the [CI/CD](https://docs.gitlab.com/ee/ci/README.html) process [here](https://gitlab.com/MoritzBru/cartography-playground/pipelines).


If you want to make a local *production* build and push the file manually to another server you first have to adapt the target url in the [\_cofing.yml](https://gitlab.com/MoritzBru/cartography-playground/blob/master/_config.yml) by changing the `baseurl` and `url`. Then you have to run following command (on Windows)

```console
& { $JEKYLL_ENV = 'production'; $env:JEKYLL_ENV = 'production'; bundle exec jekyll serve }
```

You can find the minified and compressed output in the `_site` folder.
