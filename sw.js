importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.4.1/workbox-sw.js');

workbox.routing.registerRoute(
  new RegExp('https://fonts.(?:googleapis|gstatic).com/(.*)'),
  workbox.strategies.staleWhileRevalidate({
    cacheName: 'googleapis',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 30,
      }),
    ],
  })
);

workbox.routing.registerRoute(
  /\/$/,
  workbox.strategies.staleWhileRevalidate({
    cacheName: 'html',
  })
);

workbox.routing.registerRoute(
  /\.(?:js|css|svg|png)$/,
  workbox.strategies.staleWhileRevalidate({
    cacheName: 'resources',
  })
);

// self.addEventListener('beforeinstallprompt', (e) => {
//   e.preventDefault();
//   e.prompt();
//   e.userChoice
//   .then((choiceResult) => {
//     if (choiceResult.outcome === 'accepted') {
//       console.log('User accepted the A2HS prompt');
//     } else {
//       console.log('User dismissed the A2HS prompt');
//     }
//   });
// });
