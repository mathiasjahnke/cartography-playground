# inspired by https://github.com/digitalsparky/jekyll-minifier

require 'uglifier'

module Jekyll
  module Compressor

    def output_file(dest, content)
      FileUtils.mkdir_p(File.dirname(dest))
      File.open(dest, 'w') do |f|
        f.write(content)
      end
    end

    def output_js(path, content)
      compressed = Uglifier.new()
      output_file(path, compressed.compile(content))
    end

  end

  class StaticFile
    include Compressor

    def copy_file(path, dest_path)
      FileUtils.mkdir_p(File.dirname(dest_path))
      FileUtils.cp(path, dest_path)
    end

    def write(dest)
      dest_path = destination(dest)

      return false if File.exist?(dest_path) and !modified?
      self.class.mtimes[path] = mtime

      if ENV["JEKYLL_ENV"] == "production" && File.extname(dest_path).eql?('.js')
        if dest_path.end_with?('.min.js')
          copy_file(path, dest_path)
        else
          output_js(dest_path, File.read(path))
        end
      else
        copy_file(path, dest_path)
      end

      true
    end

  end

end
