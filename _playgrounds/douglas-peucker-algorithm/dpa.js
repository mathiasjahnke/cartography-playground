//==============================================================================
//  General
//==============================================================================

var data_orig = [];
var data_simple = [];

var dpa_button_clear = document.getElementById("dpa-clear");
var dpa_button_reset = document.getElementById("dpa-reset");
var dpa_button_reset_spiral = document.getElementById("dpa-reset-spiral");
var dpa_button_reset_zigzag = document.getElementById("dpa-reset-zigzag");

var eps_range = document.getElementById("eps-range");
var eps_value = document.getElementById("eps-value");

var nr_points_orig = document.getElementById("nr-points-orig");
var nr_points_simple = document.getElementById("nr-points-simple");
var nr_points_percentage = document.getElementById("nr-points-percentage");

var draw = SVG("dpa-svg");

var polyline_orig = SVG.get('#polyline-orig');
var points_orig = SVG.get('#points-orig');
var polyline_simple = SVG.get('#polyline-simple');
var points_simple = SVG.get('#points-simple');

//==============================================================================
//  Setup
//==============================================================================

draw.node.addEventListener("click", function (e) {addNewPoint(getPointClickedOn(e));}, false);

dpa_button_clear.addEventListener("click", function () {clearAll();}, false);
dpa_button_reset.addEventListener("click", function () {loadData(default_points);}, false);
dpa_button_reset_spiral.addEventListener("click", function () {loadData(default_points_spiral);}, false);
dpa_button_reset_zigzag.addEventListener("click", function () {loadData(default_points_zigzag);}, false);

eps_range.addEventListener("input", function () {updateAll();}, false);

window.addEventListener('load', function () {loadData(default_points);}, false);

//==============================================================================
//  Data
//==============================================================================

var default_points = [{x:10, y:60}, {x:12, y:46}, {x:15, y:36}, {x:23, y:29}, {x:28, y:34}, {x:30, y:46}, {x:30, y:56}, {x:27, y:66}, {x:26, y:76}, {x:30, y:85}, {x:34, y:87}, {x:40, y:78}, {x:42, y:72}, {x:43, y:60}, {x:44, y:51}, {x:46, y:38}, {x:46, y:26}, {x:49, y:17}, {x:58, y:11}, {x:63, y:24}, {x:61, y:38}, {x:60, y:55}, {x:59, y:67}, {x:58, y:82}, {x:64, y:90}, {x:68, y:91}, {x:79, y:89}, {x:84, y:82}, {x:85, y:70}, {x:83, y:60}, {x:80, y:50}, {x:79, y:36}, {x:82, y:26}, {x:86, y:20}, {x:94, y:17}, {x:113, y:15}, {x:127, y:19}, {x:133, y:28}, {x:134, y:40}, {x:134, y:53}, {x:131, y:62}, {x:124, y:72}, {x:116, y:76}, {x:105, y:77}, {x:99, y:73}, {x:94, y:63}, {x:95, y:50}, {x:102, y:41}, {x:111, y:34}, {x:123, y:42}, {x:122, y:53}, {x:116, y:61}, {x:109, y:64}, {x:106, y:58}];

var default_points_spiral = [{x:24.4, y:19.4}, {x:15.4, y:32.9}, {x:13.3, y:65.0}, {x:19.4, y:85.2}, {x:50.4, y:90.9}, {x:88.9, y:90.4}, {x:112.1, y:87.7}, {x:126.1, y:70.0}, {x:131.1, y:59.4}, {x:130.8, y:38.5}, {x:127.4, y:26.2}, {x:116.3, y:18.0}, {x:90.4, y:17.2}, {x:67.9, y:19.7}, {x:52.6, y:26.7}, {x:41.4, y:33.2}, {x:35.1, y:40.7}, {x:33.4, y:52.5}, {x:37.8, y:61.9}, {x:44.9, y:68.2}, {x:61.1, y:74.9}, {x:81.9, y:78.9}, {x:96.9, y:76.9}, {x:108.6, y:68.7}, {x:113.8, y:59.0}, {x:114.8, y:48.9}, {x:112.8, y:40.4}, {x:103.1, y:33.9}, {x:85.6, y:33.7}, {x:70.9, y:34.5}, {x:59.8, y:41.2}, {x:56.9, y:47.5}, {x:57.9, y:54.9}, {x:64.6, y:60.5}, {x:76.8, y:63.9}, {x:90.4, y:61.2}, {x:96.4, y:55.5}, {x:97.1, y:49.5}, {x:86.6, y:45.0}, {x:77.4, y:44.9}, {x:74.6, y:49.2}, {x:78.3, y:52.9}, {x:83.8, y:53.0}];

var default_points_zigzag = [{x:12.8, y:49.5}, {x:18.4, y:40.0}, {x:19.6, y:30.2}, {x:20.8, y:22.7}, {x:25.1, y:16.9}, {x:29.1, y:14.7}, {x:33.3, y:18.4}, {x:34.4, y:25.7}, {x:33.8, y:35.5}, {x:32.1, y:44.7}, {x:30.6, y:53.5}, {x:30.6, y:61.5}, {x:31.1, y:67.5}, {x:32.4, y:76.4}, {x:34.4, y:81.0}, {x:39.6, y:83.7}, {x:44.6, y:82.7}, {x:49.3, y:74.0}, {x:49.4, y:61.4}, {x:50.1, y:47.4}, {x:51.6, y:36.0}, {x:53.3, y:26.5}, {x:57.4, y:21.9}, {x:64.1, y:22.4}, {x:66.1, y:31.0}, {x:65.8, y:42.7}, {x:63.6, y:53.0}, {x:61.4, y:62.5}, {x:63.3, y:77.2}, {x:63.8, y:80.9}, {x:68.9, y:81.5}, {x:76.8, y:76.2}, {x:78.8, y:64.9}, {x:78.1, y:51.2}, {x:78.8, y:41.0}, {x:80.4, y:30.5}, {x:82.6, y:24.7}, {x:93.6, y:30.4}, {x:95.1, y:42.4}, {x:91.6, y:54.0}, {x:91.3, y:65.9}, {x:91.9, y:73.4}, {x:94.8, y:77.7}, {x:100.6, y:77.9}, {x:105.6, y:68.0}, {x:106.4, y:57.7}, {x:107.6, y:47.0}, {x:107.8, y:37.9}, {x:111.3, y:30.2}, {x:114.1, y:28.0}, {x:123.4, y:31.0}, {x:126.9, y:43.7}, {x:125.8, y:52.9}, {x:121.4, y:63.5}, {x:120.9, y:72.0}, {x:126.3, y:76.9}, {x:132.3, y:75.5}, {x:137.9, y:66.0}, {x:139.6, y:56.5}, {x:140.3, y:49.2}];

//==============================================================================
//  simplify.js
//==============================================================================

// mourner.github.io/simplify-js

// square distance between 2 points
function getSqDist(p1, p2) {

  var dx = p1.x - p2.x;
  var dy = p1.y - p2.y;

  return dx * dx + dy * dy;
}

// square distance from a point to a segment
function getSqSegDist(p, p1, p2) {

  var x = p1.x;
  var y = p1.y;
  var dx = p2.x - x;
  var dy = p2.y - y;

  if (dx !== 0 || dy !== 0) {

    var t = ((p.x - x) * dx + (p.y - y) * dy) / (dx * dx + dy * dy);

    if (t > 1) {
      x = p2.x;
      y = p2.y;
    } else if (t > 0) {
      x += dx * t;
      y += dy * t;
    }
  }

  dx = p.x - x;
  dy = p.y - y;

  return dx * dx + dy * dy;
}
// rest of the code doesn't care about point format

// basic distance-based simplification
function simplifyRadialDist(points, sqTolerance) {

  var prevPoint = points[0];
  var newPoints = [prevPoint];
  var point;

  for (var i = 1, len = points.length; i < len; i++) {
    point = points[i];

    if (getSqDist(point, prevPoint) > sqTolerance) {
      newPoints.push(point);
      prevPoint = point;
    }
  }

  if (prevPoint !== point) newPoints.push(point);

  return newPoints;
}

function simplifyDPStep(points, first, last, sqTolerance, simplified) {
  var maxSqDist = sqTolerance;
  var index;

  for (var i = first + 1; i < last; i++) {
    var sqDist = getSqSegDist(points[i], points[first], points[last]);

    if (sqDist > maxSqDist) {
      index = i;
      maxSqDist = sqDist;
    }
  }

  if (maxSqDist > sqTolerance) {
    if (index - first > 1) simplifyDPStep(points, first, index, sqTolerance, simplified);
    simplified.push(points[index]);
    if (last - index > 1) simplifyDPStep(points, index, last, sqTolerance, simplified);
  }
}

// simplification using Ramer-Douglas-Peucker algorithm
function simplifyDouglasPeucker(points, sqTolerance) {
  var last = points.length - 1;

  var simplified = [points[0]];
  simplifyDPStep(points, 0, last, sqTolerance, simplified);
  simplified.push(points[last]);

  return simplified;
}

// both algorithms combined for awesome performance
function simplify(points, tolerance, highestQuality) {

  if (points.length <= 2) return points;

  var sqTolerance = tolerance !== undefined ? tolerance * tolerance : 1;

  points = highestQuality ? points : simplifyRadialDist(points, sqTolerance);
  points = simplifyDouglasPeucker(points, sqTolerance);

  return points;
}

//==============================================================================
//  Functionality
//==============================================================================

// General / Drawing

function getPointClickedOn(e) {
  // cursor point in display coordinates
  var pt_client = draw.node.createSVGPoint();
  pt_client.x = e.clientX;
  pt_client.y = e.clientY;
  // cursor point, translated into svg coordinates
  var pt_svg = pt_client.matrixTransform(draw.node.getScreenCTM().inverse());
  return {x:pt_svg.x, y:pt_svg.y};
}

function addNewPoint(pt) {
  data_orig.push(pt);
  data_simple = copy(data_orig);
  update_range(0);
  redrawAll();
}

function drawPoint(pt, parent) {
  parent.circle(1.2).attr({ cx: pt.x, cy: pt.y });
}

function redrawAll() {
  // clear
  polyline_orig.clear();
  points_orig.clear();
  polyline_simple.clear();
  points_simple.clear();
  // draw
  polyline_orig.polyline(obj2arr(data_orig));
  data_orig.map(function(pt){return drawPoint(pt, points_orig);});
  polyline_simple.polyline(obj2arr(data_simple));
  data_simple.map(function(pt){return drawPoint(pt, points_simple);});
  // update text
  nr_points_orig.innerHTML = data_orig.length;
  nr_points_simple.innerHTML = data_simple.length;
  nr_points_percentage.innerHTML = data_orig.length != 0 ? (100 - data_simple.length / data_orig.length * 100.0).toFixed(0) : "0";
}

function clearAll() {
  data_orig = [];
  data_simple = [];
  update_range(0);
  redrawAll();
}

function loadData(data) {
  data_orig = copy(data);
  data_simple = copy(data_orig);
  update_range(0);
  redrawAll();
}

// Simplification

function update_range(update) {
  update = typeof update !== 'undefined' ? update : 1;
  if (update == 0) {
    eps_range.value = 0;
    eps_value.innerHTML = "00.0";
  } else {
    eps_value.innerHTML = parseFloat(eps_range.value).toFixed(1).padStart(4, "0");
  }
  return eps_range.value;
}

function updateAll() {
  var eps = update_range();
  data_simple = simplify(data_orig, eps, false);
  redrawAll();
}

//==============================================================================
//  Helper
//==============================================================================

function copy(a) {
  return JSON.parse(JSON.stringify(a));
}

function obj2arr(o) {
  var output = o.map(function (a) {
    return [a.x, a.y];
  });
  return output;
}

function printDataPoints() {
  var output = "[";
  data_orig.forEach(function (p) {
    output += "{x:" + p.x.toFixed() + ",y:" + p.y.toFixed() + "}, ";
  });
  output = output.slice(0, -2);
  output += "]";

  return output;
}
