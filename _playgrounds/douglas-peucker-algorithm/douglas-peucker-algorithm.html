---
title: "Douglas-Peucker algorithm"
description: "Play with the algorithms parameter to reduce a piecewise linear curve."
date: 2018-02-21
tags: douglas peucker ramer line smoothing simplification generalization generalisation
references:
  - author: "Wikipedia"
    title: "Ramer–Douglas–Peucker algorithm"
    date: February 2018
    link: https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm
  - author: "Urs Ramer"
    title: "An iterative procedure for the polygonal approximation of plane curves. In Computer Graphics and Image Processing. Volume 1, Issue 3, Pages 244-256"
    date: 1972
    link: https://www.sciencedirect.com/science/article/pii/S0146664X72800170?via%3Dihub
  - author: "David Douglas, Thomas Peucker"
    title: "Algorithms for the reduction of the number of points required to represent a digitized line or its caricature. In Cartographica: The International Journal for Geographic Information and Geovisualization. Volume 10, Issue 2, Pages 112–122"
    date: 1973
    link: https://www.researchgate.net/publication/242506399_Algorithms_for_the_Reduction_of_the_Number_of_Points_Required_to_Represent_a_Digitized_Line_or_Its_Caricature
  - author: "Vladimir Agafonkin"
    title: "Simplify.js"
    date: 2017
    link: https://github.com/mourner/simplify-js
foot_additional: >
  {% include mathjax.html %}
  {% include svgjs.html %}
  <script src="{{ 'dpa.js' | prepend: page.url | absolute_url }}"></script>
---

<div class="row">
  <div class="col">
    <p>
      The <em>Douglas–Peucker algorithm</em>, also known as <em>Ramer–Douglas–Peucker algorithm</em> or <em>iterative end-point fit algorithm</em> is an algorithm to smooth polylines (lines that are composed of linear line segments) by reducing the number of points. The simplified curve should preserve the rough shape of the original curve but consist only of a subset of the points that defined the original curve.<br/>
      The degree of coarsening is controlled by a single parameter ε, which defines the maximum distance between the original points and the simplified curve.<br/>
      The algorithm was developed independently by Urs Ramer in 1972 and  by David Douglas and Thomas Peucker in 1973.
    </p>
  </div>
</div>

<h2 class="mt-5">Algorithm</h2>
<hr>
<p>
  Given is the starting curve \( C \) as an ordered set of \( n \) points
  \[ C = (P_1, P_2, P_3, \cdots, P_n) \]
  and the distance dimension \( \varepsilon \) with
  \[ \varepsilon > 0 .\]
</p>
{% include_relative imgs/dpa-imgs-01.svg class="img-fluid mx-auto d-block" %}
<p>
  The first approximation of \( C \) is the line segment \( \overline{P_1P_n} \). The algorithm marks the first and last point to be kept.<br/>
  Now all \( n-2 \) inner points are looked at, to find the point \( P_m \) that is furthest from that line segment with
  \[ d_{max} = \underset{i=2 \cdots n-1}{\max} d(P_i, \overline{P_1P_n}). \]
  If \( d_{max} \) is within the tolerance
  \[ d_{max} \leq \varepsilon \]
  then the simplification is finished and all inner points can be discarded without the simplified curve being worse than \( \varepsilon \).
</p>
{% include_relative imgs/dpa-imgs-02.svg class="img-fluid mx-auto d-block" %}
<p>
  Else, \( P_m \) must be kept and the algorithm recursivly processes the new parts
  \[ C_1 = (P_1, \cdots, P_m) \]
  and
  \[ C_2 = (P_m, \cdots, P_n) .\]
</p>
{% include_relative imgs/dpa-imgs-03.svg class="img-fluid mx-auto d-block" %}
<p>
  Those parts are then again processed recursivly.
</p>
{% include_relative imgs/dpa-imgs-04.svg class="img-fluid mx-auto d-block" %}
{% include_relative imgs/dpa-imgs-05.svg class="img-fluid mx-auto d-block" %}
<p>
  When the recursion is complete, the output curve is the set of all kept points.
</p>
{% include_relative imgs/dpa-imgs-06.svg class="img-fluid mx-auto d-block" %}

<h2 class="mt-5">Hands-On</h2>
<hr>
<div class="container mw-900">
  <div class="row mb-3">
    <div class="col">Drag the Slider to change the value of ε and simplify the drawn curve. Click in the grid to draw a new point. You can clear the curve drawing by clicking on the <em>Clear</em> Button or you can restore a default Path by choosing one from the <em>Reset</em> Button.</div>
  </div>

  <div class="card shadow">
    <div class="card-header h5">
      Douglas-Peucker algorithm
    </div>
    <div class="card-body p-3">
      <div class="row mb-2 d-flex align-items-center text-monospace">
        <div class="col-sm-4">
          Tolerance&nbsp;ε
          <span class="badge badge-pill badge-primary ml-3">
            <span class="lead" id="eps-value">0</span>
          </span>
        </div>
        <div class="col-sm-8">
          <label for="eps-range" class="sr-only">Epsilon range</label>
          <input id="eps-range" class="custom-range m-1" type="range" min="0" max="25" step="0.1" value="0"/>
        </div>
      </div>
      <div class="row d-flex align-items-center">
        <div class="col-sm-7 text-monospace">
          Number of Points reduced from&nbsp;<span id="nr-points-orig" class="font-weight-bold text-dark">0</span>&nbsp;to&nbsp;<span id="nr-points-simple" class="font-weight-bold text-primary">0</span> (<span id="nr-points-percentage"></span>%)
        </div>
        <div class="col-sm-5 text-right">
          <button type="button" id="dpa-clear" class="btn btn-sm btn-warning m-1"><i class="mdi mdi-delete mr-1"></i>Clear</button>
          <div class="btn-group m-1">
            <button type="button" id="dpa-reset" class="btn btn-sm btn-secondary border-right"><i class="mdi mdi-undo-variant mr-1"></i>Reset</button>
            <button type="button" class="btn btn-sm btn-secondary border-left dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
              <span class="sr-only">Toggle Dropdown</span>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <button class="dropdown-item dpa-reset-dropdown" id="dpa-reset-spiral">Spiral</button>
              <button class="dropdown-item dpa-reset-dropdown" id="dpa-reset-zigzag">Zigzag</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    {% include_relative dpa.svg class="card-img-bottom border-top cursor-crosshair bg-light" %}
  </div>

</div>
