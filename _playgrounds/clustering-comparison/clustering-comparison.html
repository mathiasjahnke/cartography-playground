---
title: "Clustering comparison"
description: "Compare the centroid-based clustering method k-Means and the density based method DBSCAN."
date: 2018-04-17
tags: clustering cluster comparison kmeans k-means centroid dbscan density
references:
  - author: "Jakub Młokosiewicz"
    title: "k-means-visualization on GitHub"
    date: April 2018
    link: https://github.com/hckr/k-means-visualization
  - author: "Wikipedia"
    title: "k-means clustering"
    date: April 2018
    link: https://en.wikipedia.org/wiki/K-means_clustering
  - author: "James MacQueen"
    title: "Some methods for classification and analysis of multivariate observations"
    date: 1967
    link: https://projecteuclid.org/euclid.bsmsp/1200512992
  - author: "Corneliu Sugar"
    title: "jDBSCAN on GitHub"
    date: April 2018
    link: https://github.com/upphiminn/jDBSCAN
  - author: "Wikipedia"
    title: "DBSCAN"
    date: April 2018
    link: https://en.wikipedia.org/wiki/DBSCAN
  - author: "Martin Ester, Hans-Peter Kriegel, Jörg Sander, Xiaowei Xu"
    title: "A density-based algorithm for discovering clusters in large spatial databases with noise"
    date: 1996
    link: http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.121.9220
head_additional: >
  <style>
    #btn-kmeans-run-stop.btn-secondary .kmeans-run {display: inherit;}
    #btn-kmeans-run-stop.btn-secondary .kmeans-stop {display: none;}
    #btn-kmeans-run-stop.btn-warning .kmeans-run {display: none;}
    #btn-kmeans-run-stop.btn-warning .kmeans-stop {display: inherit;}
  </style>
foot_additional: >
  {% include mathjax.html %}
  {% include svgjs.html %}
  <script src="{{ 'clustering.js' | prepend: page.url | absolute_url }}"></script>
---

<div class="row">
  <div class="col">
    <p>On this page the centroid-based clustering method <em>k-Means</em> will be compared to the density-based clustering method <em>DBSCAN</em>.</p>
    <p>k-Means was introduced by James MacQueen in 1967. It aims to partition all observations in to k clusters so that the within-cluster sum of squares is minimized while the between-cluster sum of squares is maximized.</p>
    <p>DBSCAN or <strong>D</strong>ensity-<strong>b</strong>ased <strong>s</strong>patial <strong>c</strong>lustering of <strong>a</strong>pplications with <strong>n</strong>oise was introduced in 1996 by Martin Ester, Hans-Peter Kriegel, Jörg Sander and Xiaowei Xu. It forms clusters of closely neighboring points and is able to detect outliers as noise.</p>
  </div>
</div>

<h2 class="mt-5">Algorithms</h2>
<hr>
<div class="row">
  <div class="col-md">
    <h3 class="mt-3">k-Means</h3>

    <p>
      The k-Means Algorithm is based on the method of least squares and optimizes
      \[ F = \sum_{i=1}^{k}\sum_{x_{j} \in S_{i}}\left \| x_{j} - \mu_{i}  \right \|^{2} \]
      where \(x_i\) are data points, \(\mu_i\) are Centroids or Means and \(S_i\) are corresponding clusters.<br>
      The Algorithm has three steps.
    </p>
    <p>
      <strong>1. Initialisation</strong><br>
      Choose \(k\) Centroids (also called Means)<br>
      \( m_1^{(0)},\cdots,m_k^{(0)} \)
    </p>
    <img class="img-fluid mx-auto d-block" src="imgs/kmeans_1.svg" alt="kmeans initialisation">
    <p>
      <strong>2. Assign Points</strong><br>
      Each data Point is assigned to the "nearest" cluster (cluster variance is increased the least).<br>
      \( S_i^{\left( t \right)} = \left\{ x_j : \left\| x_j - m_{i}^{\left( t \right)} \right\|^2 \leq \left\| x_j - m_{i^*}^{\left( t \right)} \right\|^2 \text{ for all } i^*=1,\cdots,k \right\} \)
    </p>
    <img class="img-fluid mx-auto d-block" src="imgs/kmeans_2.svg" alt="kmeans assign points">
    <p>
      <strong>3. Update Centroids</strong><br>
      Recalculate the new cluster means.<br>
      \( m_i^{\left( t+1 \right)} = \frac{1}{\left| S_{i}^{\left( t \right)} \right|} \sum_{x_j \in S_{i}^{\left( t \right)}} x_j \)
    </p>
    <img class="img-fluid mx-auto d-block" src="imgs/kmeans_3.svg" alt="kmeans update centroids">
    <p>
      Repeat steps 2 and 3 until the centroids don't change anymore.
    </p>
    <img class="img-fluid mx-auto d-block" src="imgs/kmeans_4.svg" alt="kmeans iteration">
  </div>
  <div class="col-md">
    <h3 class="mt-3">DBSCAN</h3>
    <p>
      In DBSCAN there are three different kinds of points:
      <ul>
        <li><strong>core points</strong> which are dense themselfs</li>
        <li><strong>density-reachable points</strong> that are reachable from core points but are not dense themselfs</li>
        <li><strong>outliers</strong> or noise points</li>
      </ul>
      </p>
      <p>The DBSCAN method has two parameters <em>ε</em> and <em>minPts</em>, where <em>ε</em> is the maximum neigborhood radius and <em>minPts</em> is the minimum number of neighbors to be a core point.</p>
      <p>From one point another point is reachable if their distance is smaller than <em>ε</em>. A Point is dense (=core point) if it has at least <em>minPts</em> in its <em>ε</em>-reachable neighborhood.</p>
      <p>Density-reachable points are reachable from core points but are not themselfs dense.</p>
      <p>All reachable points around at least one core point form a cluster. Other points are outliers.</p>
      <figure class="figure text-center d-block my-4">
        {% include_relative imgs/dbscan.svg class="figure-img img-fluid" %}
        <figcaption class="figure-caption">blue: core points, green: density-reachable points, orange: outliers</figcaption>
      </figure>
      <p><strong>Algorithm</strong>
        <ol>
          <li>Start with arbitrary, not yet visited point</li>
          <li>Get the point's <em>ε</em>-neighborhood</li>
          <li>If the point is dense, start a cluster</li>
          <li>If not, the point is labeled as noise (it can get part of a cluster later)</li>
          <li>Get the <em>ε</em>-neighborhood of all unvisited points in the cluster and add them to the cluster</li>
          <li>Again add the <em>ε</em>-neighborhood of the neighbors to the cluster if the neighbor is dense itself</li>
          <li>Continue until the density-connected cluster is completely found</li>
          <li>Start with a new unvisited point</li>
          <li>Continue until all points are either part of a cluster or labeled as noise</li>
        </ol>
      </p>
      <p><span class="text-secondary">Advantages of DBSCAN over k-Means</span>
        <ul>
          <li>DBSCAN does not require a priori knowled about the number of classes</li>
          <li>DBSCAN can separate arbitrarily shaped and non-linearly separable clusters</li>
          <li>DBSCAN is robust against noise</li>
        </ul>
      </p>
      <p><span class="text-warning">Disadvantages of DBSCAN</span>
        <ul>
          <li>One has to understand the data to choose meaningful parameters</li>
          <li>DBSCAN has difficulties at clustering datasets with large differences in densities</li>
          <li>DBSCAN is not entirely deterministic (Assignment of points reachable from more than one cluster is dependant on the processing order)</li>
        </ul>
      </p>
  </div>
</div>

<h2 class="mt-5">Hands-On</h2>
<hr>
<div class="container mw-900">
  <p>
    There are three tabs <em>Data</em>, <em>k-Means</em> and <em>DBSCAN</em>.<br>
    In the <em>Data</em> tab you can toggle to draw own data points by clicking in the drawing area below, you can load random and predefined datasets and clear the drawing area.<br>
    In the <em>k-Means</em> you can change the configuration of k-Means by adding own centroids, adding a number of random centroids and clearing all centroids. In addition you can change the distance measure. Then you can run the k-Means algorithm either step by step or in a loop.<br>
    In the <em>DBSCAN</em> you can change the parameters <em>ε</em> and <em>minPts</em> and choose the distance measure. Then you can perform the calssification and clear the clustering again.
  </p>

  <div class="card shadow">
    <div class="card-header">
      <nav>
        <div class="nav nav-tabs nav-justified card-header-tabs" id="nav-tab" role="tablist">
          <a class="nav-item nav-link active" id="nav-data-tab" data-toggle="tab" role="tab" href="#nav-data">Data</a>
          <a class="nav-item nav-link" id="nav-kmeans-tab" data-toggle="tab" role="tab" href="#nav-kmeans">k-Means</a>
          <a class="nav-item nav-link" id="nav-dbscan-tab" data-toggle="tab" role="tab" href="#nav-dbscan">DBSCAN</a>
        </div>
      </nav>
    </div><!-- card-header -->
    <div class="card-body p-3">
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-data" role="tabpanel">
          <div class="row d-flex align-items-center">
            <div class="col-md my-1">
              <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="check-draw-points">
                <label class="custom-control-label" for="check-draw-points">Draw Data Points</label>
              </div>
            </div>
            <div class="col-md my-1">
              <button type="button" class="btn btn-sm btn-block btn-secondary dropdown-toggle" data-toggle="dropdown"><i class="mdi mdi-plus-circle mr-1"></i>Add Dataset</button>
              <div class="dropdown-menu dropdown-menu-right">
                <button class="dropdown-item" id="btn-data-clusters">3 Clusters</button>
                <button class="dropdown-item" id="btn-data-smiley">Smiley</button>
                <button class="dropdown-item" id="btn-data-grid">Grid 15 Clusters</button>
                <div class="dropdown-divider"></div>
                <button class="dropdown-item" id="btn-data-random">Random Noise</button>
                <button class="dropdown-item" id="btn-data-random-clusters">Random Clusters</button>
              </div>
            </div>
            <div class="col-md my-1">
              <button type="button" id="btn-data-clear" class="btn btn-sm btn-block btn-warning"><i class="mdi mdi-delete mr-1"></i>Clear Points</button>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="nav-kmeans" role="tabpanel">
          <div class="card-deck">
            <div class="card">
              <h6 class="card-header">Configuration</h6>
              <div class="card-body p-3">
                <div class="custom-control custom-switch mb-2">
                  <input type="checkbox" class="custom-control-input" id="check-draw-centroids">
                  <label class="custom-control-label" for="check-draw-centroids">Draw Centroids</label>
                </div>
                <div class="input-group input-group-sm mb-2">
                  <div class="input-group-prepend">
                    <button class="btn btn-secondary btn-sm" id="btn-centroids-add" type="button"><i class="mdi mdi-plus-box mr-1"></i>Add random Centroids</button>
                  </div>
                  <select class="custom-select custom-select-sm" id="sel-centroids-num"></select>
                </div>
                <button type="button" id="btn-centroids-clear" class="btn btn-sm btn-warning btn-block mb-2"><i class="mdi mdi-delete mr-1"></i>Clear Centroids</button>
                <div class="input-group input-group-sm">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="sel-distance-kmeans">Distance Measure</label>
                  </div>
                  <select class="custom-select custom-select-sm" id="sel-distance-kmeans">
                    <option value="euclideanDistance">Euclidean</option>
                    <option value="manhattanDistance">Manhattan</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="card">
              <h6 class="card-header">Algorithm</h6>
              <div class="card-body p-3">
                <button type="button" id="btn-kmeans-assign-points" class="btn btn-block btn-sm btn-primary mb-2">Assign Points</button>
                <button type="button" id="btn-kmeans-update-centroids" class="btn btn-block btn-sm btn-primary mb-2" disabled>Update Centroids</button>
                <button type="button" id="btn-kmeans-run-stop" class="btn btn-block btn-sm btn-secondary">
                  <span class="kmeans-run"><i class="mdi mdi-play mr-1"></i>Run k-Means</span>
                  <span class="kmeans-stop"><i class="mdi mdi-loading mdi-spin mr-1"></i>Stop k-Means</span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="nav-dbscan" role="tabpanel">
          <div class="card-deck">
            <div class="card">
              <h6 class="card-header">Configuration</h6>
              <div class="card-body p-3">
                <form class="form-inline mb-2">
                  <label class="mr-5" for="rg-eps">ε:<span class="text-primary text-monospace ml-2" id="label-eps"></span></label>
                  <input type="range" class="custom-range" min="0" max="70" value="30" id="rg-eps">
                </form>
                <form class="form-inline mb-3">
                  <label class="mr-5" for="rg-minpts">minPts:<span class="text-primary text-monospace ml-2" id="label-minpts"></span></label>
                  <input type="range" class="custom-range" min="1" max="10" value="3" id="rg-minpts">
                </form>
                <div class="input-group input-group-sm">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="sel-distance-dbscan">Distance Measure</label>
                  </div>
                  <select class="custom-select custom-select-sm" id="sel-distance-dbscan">
                    <option value="EUCLIDEAN">Euclidean</option>
                    <option value="MANHATTAN">Manhattan</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="card">
              <h6 class="card-header">Algorithm</h6>
              <div class="card-body p-3">
                <button type="button" id="btn-dbscan-run" class="btn btn-sm btn-secondary btn-block mb-2"><i class="mdi mdi-play mr-1"></i>Run DBSCAN</button>
                <button type="button" id="btn-dbscan-clear" class="btn btn-sm btn-warning btn-block"><i class="mdi mdi-delete mr-1"></i>Clear Clustering</button>
              </div>
            </div>
          </div>
        </div>
      </div><!-- tab-content -->
    </div><!-- card-body -->
    <div id="drawing" class="card-img-bottom bg-light border-top"></div>
  </div><!-- card -->

</div>
