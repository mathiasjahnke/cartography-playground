---
title: "Map design"
description: "Learn and fiddle around with some general design principles for map design - especially color and font."
date: 2018-04-09
tags: map design color font text mapbox
references:
  - author: "Mapbox"
    title: "The Guide to Map Design"
    date: April 2018
    link: https://www.mapbox.com/map-design/
  - author: "Eduard Imhof"
    title: "Cartographic Relief Presentation"
    date: 2007
    link: https://books.google.bg/books?id=cVy1Ms43fFYC&printsec=frontcover
  - author: "Günter Hake, Dietmar Grünreich, Liqiu Meng"
    title: "Kartographie: Visualisierung raum-zeitlicher Informationen"
    date: 1994
    link: https://books.google.de/books/about/Kartographie.html?id=MsggAAAAQBAJ&printsec=frontcover&source=kp_read_button&redir_esc=y#v=onepage&q&f=false
head_additional: >
  <style>
    #map {height: 600px;}
    .tab-pane .row {min-height: 35px;}
    .no-caret::after {display: none;}
  </style>
  {% include mapbox.html geocoder=true %}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css" integrity="sha256-f83N12sqX/GO43Y7vXNt9MjrHkPc4yi9Uq9cLy1wGIU=" crossorigin="anonymous" />
  <link rel="stylesheet" href="{{ 'sp-cp.css' | prepend: page.url | absolute_url }}">
  <link href="https://fonts.googleapis.com/css?family=Caveat|Merriweather|Montserrat|Open+Sans|Orbitron|Oswald|Playfair+Display|Source+Code+Pro|Ubuntu+Mono" rel="stylesheet">
foot_additional: >
  <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js" integrity="sha256-ZdnRjhC/+YiBbXTHIuJdpf7u6Jh5D2wD5y0SNRWDREQ=" crossorigin="anonymous"></script>
  <script>var mapbox_style = "{{ 'style.json' | prepend: page.url | absolute_url }}"</script>
  <script src="{{ 'mapdesign.js' | prepend: page.url | absolute_url }}"></script>
---

<div class="row my-5">
  <div class="col">
    <blockquote class="blockquote">
      <p class="blockqoute-content">The map is a graphic creation. Even when it is so highly conditioned by scientific purpose, it cannot escape graphic laws.</p>
      <footer class="blockquote-footer">Eduard Imhof in <cite title="Source Title">Cartographic Relief Presentation</cite></footer>
    </blockquote>
  </div>
</div>

<div class="row">
  <div class="col-md-5">
    <p class="font-weight-bold mb-1">What should your map be like?</p>
    <ul>
      <li>Clear</li>
      <li>Catchy</li>
      <li>Interesting</li>
      <li>Easy to read</li>
      <li>Uncomplicated to understand</li>
      <li>Adapted to the user group</li>
    </ul>
  </div>
  <div class="col-md-7">
    <p class="font-weight-bold mb-1">Why does good map design matter?</p>
    <ul>
      <li>Tell the story the map is supposed to tell</li>
      <li>Keep users visually connected to the brand or app the map is integrated to</li>
      <li>Guide the user</li>
      <li>Highlight or deemphasise information</li>
      <li>Have a decent basemap that keeps the user oriented but doesn't distract from the main data and interactions</li>
    </ul>
  </div>
</div>

<h2 class="mt-5">Design principles</h2>
<hr>

<div class="row">
  <div class="col">
    <p>The first and most important design rule is: <strong>Rules are made to be broken</strong>. There are no hard rules on this so take all the following as suggestions.</p>
  </div>
</div>

<div class="row">
  <div class="col">
      <p class="font-weight-bold mb-1">Design elements</p>
      <p>Symbols, color and typography are the main customizable elements of the (base)map.</p>

      <figure class="figure text-center d-block">
        <img src="imgs/mapdesign_symbols.svg" class="figure-img img-fluid" alt="mapdesign symbols">
        <figcaption class="figure-caption">Example of <a class="link-underline" href="https://www.mapbox.com/maki-icons/" target="_blank" rel="noopener">Mapbox Maki Symbols</a></figcaption>
      </figure>

  </div>
</div>

<div class="row">
  <div class="col">

    <p class="font-weight-bold mb-1">General principles</p>
    <ul>
      <li>Besides the basemap, use at most three thematic components</li>
      <li>Distribute those components in different visual layers through the usage of
        <ul>
          <li>Points
            <ul>
              <li>Different point sizes are divided into different visual layers</li>
              <li>Small points fade into the background</li>
              <li>Large points pop into the foreground</li>
            </ul>
          </li>
          <li>Lines
            <ul>
              <li>Different line widths are divided into different visual layers</li>
              <li>Thin lines fade into the background</li>
              <li>Thick lines pop into the foreground</li>
            </ul>
          </li>
          <li>Areas
            <ul>
              <li>Mostly in the visual background</li>
              <li>Use color to highlight or deemphasise</li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
    <p class="font-weight-bold mb-1">Color</p>
    <ul>
      <li>Light colors for areas and layers in the visual background</li>
      <li>Strong dark colors for points, text and layers in the visual foreground</li>
      <li>Use assosiative colors (soil: brown, water: blue, hot temperatur: red, …)</li>
      <li>A limited color palette of <em>at most</em> 12 unique colors is best (but a larger variety of shades and tints is ok)</li>
    </ul>

    <figure class="figure text-center d-block">
      <img src="imgs/mapdesign_colors.svg" class="figure-img img-fluid" alt="mapdesign colors">
      <figcaption class="figure-caption">Color swatch with six base colors and different shades and tints</figcaption>
    </figure>

    <p class="font-weight-bold mb-1">Typography</p>
    <ul>
      <li>Text has lowest geometric information but is main explanatory element</li>
      <li>Possible components of the map text
        <ul>
          <li>Place names (München, Arcisstraße, …)</li>
          <li>Functional names (university, airport, …)</li>
          <li>Assosiative names (Englischer Garten, A99, …)</li>
          <li>Numbers (contour lines, ocean depths, …)</li>
          <li>Other (copyright, scale, …)</li>
        </ul>
      </li>
      <li>Can be in different languages with different alphabets (your font has to support that)</li>
      <li>Use fonts consistent for similar object classes</li>
      <li>Prefere light, sans-serif fonts</li>
    </ul>

    <figure class="figure text-center d-block">
      <img src="imgs/mapdesign_typography.svg" class="figure-img img-fluid" alt="mapdesign typography">
      <figcaption class="figure-caption">Typography example with serif and sans-serif font in light and bold font weight</figcaption>
    </figure>

  </div>
</div>

<h2 class="mt-5">Hands-On</h2>
<hr>

<div class="row mb-3">
  <div class="col">You can choose different tabs to alter different aspects of the map. In the <em>Layers</em> tab you can choose to hide or display different layers. By clicking on the small color patch next to the layer names in the <em>General</em>, <em>Landcover</em> or <em>Text</em> tab you can adjust the color and opacity of the respective layer. On the left side of the color picker you should see a history of choosen colors for the specific layers (although it takes a bit to synchronize). In the <em>Text</em> tab you can also change the font and size multiplier of the text layers and the language of the displayed text.</div>
</div>

<div class="card shadow">
  <div class="card-header">
    <nav>
      <div class="nav nav-tabs nav-justified card-header-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-layers-tab" data-toggle="tab" role="tab" href="#nav-layers">Layers</a>
        <a class="nav-item nav-link" id="nav-general-tab" data-toggle="tab" role="tab" href="#nav-general">General</a>
        <a class="nav-item nav-link" id="nav-landcover-tab" data-toggle="tab" role="tab" href="#nav-landcover">Landcover</a>
        <a class="nav-item nav-link" id="nav-text-tab" data-toggle="tab" role="tab" href="#nav-text">Text</a>
      </div>
    </nav>
  </div><!-- card-header -->
  <div class="card-body py-2">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-layers" role="tabpanel">
        <div class="row"></div>
      </div>
      <div class="tab-pane fade" id="nav-general" role="tabpanel">
        <div class="row"></div>
      </div>
      <div class="tab-pane fade" id="nav-landcover" role="tabpanel">
        <div class="row"></div>
      </div>
      <div class="tab-pane fade" id="nav-text" role="tabpanel">
        <div class="row">
          <div class="col-md mt-1">
            <div class="dropdown d-inline-block">
              <button class="btn btn-gray btn-sm dropdown-toggle no-caret mr-2" type="button" id="fontMenuButton" data-toggle="dropdown">Roboto</button>
              <div class="dropdown-menu" id="fontMenu"></div>
            </div>
            <span>Font</span>
          </div>
          <div class="col-md mt-1">
            <div class="dropdown d-inline-block">
              <button class="btn btn-gray btn-sm dropdown-toggle no-caret mr-2" type="button" id="sizeMenuButton" data-toggle="dropdown">1.0</button>
              <div class="dropdown-menu" id="sizeMenu"></div>
            </div>
            <span>Size factor</span>
          </div>
          <div class="col-md mt-1">
            <div class="dropdown d-inline-block">
              <button class="btn btn-gray btn-sm dropdown-toggle no-caret mr-2" type="button" id="languageMenuButton" data-toggle="dropdown">english</button>
              <div class="dropdown-menu" id="languageMenu"></div>
            </div>
            <span>Language</span>
          </div>
        </div>
      </div>
    </div><!-- tab-content -->
  </div><!-- card-body -->
  <div id="map" class="card-img-bottom border-top"></div>
</div><!-- card -->
